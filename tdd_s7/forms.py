from .models import Status
from django.forms import ModelForm, TextInput, Select

class Status_Form(ModelForm):
    class Meta:
        model = Status
        fields=[ 'name','status']
        widgets = {'name': TextInput(attrs={'class': 'form-control'}),'status': TextInput(attrs={'class': 'form-control'}),}

