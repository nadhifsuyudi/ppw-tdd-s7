from django.apps import AppConfig


class TddS7Config(AppConfig):
    name = 'tdd_s7'
