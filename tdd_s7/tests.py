from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Status
from .forms import Status_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time


# Create your tests here.
class UnitTest(TestCase):
    def test_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_index_template(self):
        response= Client().get('/')
        self.assertTemplateUsed(response,'index.html')

    def test_url_confirmation_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code, 200)

    def test_url_confirmation_template(self):
        response = Client().get('/confirmation/')
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_create_new_status(self):
        new= Status.objects.create(name = 'Ains', status = 'Yeet')
        self.assertTrue(isinstance(new,Status))
        self.assertTrue(new.__str__(),new.name)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_invalid_post(self):
        form = Status_Form({'name':'', 'status':''})
        self.assertFalse(form.is_valid())

    def test_form_new_isi(self):
        form= Status_Form({'name':'Sets', 'status':'Yeet'})
        self.assertTrue(form.is_valid())



class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)


    def tearDown(self):
        self.browser.quit()

    def test_input_todo(self):
        self.browser.get('http://localhost:8000')
        time.sleep(5)
        name = self.browser.find_element_by_id('id_name')
        name.send_keys("Nadhif")
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Yeet")
        submit = self.browser.find_element_by_name('submit')
        time.sleep(5)
        submit.click()
        time.sleep(5)
        submit = self.browser.find_element_by_name('continue')
        time.sleep(5)
        submit.click()
        time.sleep(5)




