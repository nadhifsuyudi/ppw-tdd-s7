from django.db import models
from django.forms import ModelForm

# Create your models here.

class Status(models.Model):
    name = models.CharField(max_length=30)
    status = models.TextField()

    def __str__(self):
        return self.name