from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, request, HttpResponse
from .forms import Status_Form
from .models import Status

# Create your views here.
def index(request):
    status = Status.objects.all()
    form= Status_Form()
    if (request.method=='POST'):
        form = Status_Form(request.POST)
    else:
        form= Status_Form()
    return render(request, 'index.html',{'form': form, 'status': status})

def confirmation(request):
    if request.method == 'POST':

        if 'submit' in request.POST:
            form = Status_Form(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                status = form.cleaned_data['status']
                return render(request, 'confirmation.html', {'name': name, 'status': status})
        elif 'continue' in request.POST:
            form = Status_Form(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')

    return render(request, 'confirmation.html', {'name': "", "status": ""})